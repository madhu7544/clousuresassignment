function cacheFunction(cb) {
    if (typeof cb !== 'function'){
        throw new Error("call back must be a function");
    }
    const cache={};
    function invoke(...args){
        const key = JSON.stringify(args)
        if (key in cache){
            return cache[key];
        }else{
            const result = cb(...args);
            cache[key] = result;
            return result;
        }
    }
    return invoke
}

module.exports = cacheFunction
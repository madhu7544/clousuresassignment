function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let ans=0;
    function increment(){
        ans+=1
        return ans
    }
    function decrement(){
        ans-=1
        return ans
    }
    return {increment,decrement}

}

module.exports = counterFactory
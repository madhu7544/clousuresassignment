function limitFunctionCallCount(cb, n) {

    if (typeof cb !== 'function' || typeof n !== 'number' || n <= 0 || n===undefined) {
        throw new Error('Invalid input parameters.');
      }

    let count =0;
    function invoke(...args){
        if (count < n){
            count++
            return cb.apply(null,args)
        }
         else{
            console.log(null)
            return null
         } 
    }
    return invoke
}

module.exports = limitFunctionCallCount
